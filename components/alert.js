'use strict';

const React = require('react');
const rr = require('react-redux');

class Alert extends React.Component {
  render() {
    return (
      React.createElement('h1', { onClick: this.props.onClick }, `${this.props.text} ${this.props.count}!`)
    )
  }
}

const mapStateToProps = (state) => {
  return {
    count: state.count
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onClick: () => dispatch({ type: 'INCREMENT' })
  }
}

module.exports = rr.connect(mapStateToProps, mapDispatchToProps)(Alert);
