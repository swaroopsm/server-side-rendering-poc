'use strict';

const redux = require('redux');

const reducer = (state, action) => {
  if(action.type === 'INCREMENT') {
    return Object.assign({}, state, { count: state.count + 1 });
  }

  return state;
};

module.exports = {
  init: () => {
    return redux.createStore(reducer, { count: 0 });
  }
};
