import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider  } from 'react-redux';
import reducer from './store';
import Alert from './components/alert';

const store = reducer.init();

ReactDOM.render(
  <Provider store={ store }><Alert text='1. Alert' /></Provider>,
  document.getElementById('root')
);

ReactDOM.render(
  <Provider store={ store }><Alert text='2. Alert' /></Provider>,
  document.getElementById('root2')
);
