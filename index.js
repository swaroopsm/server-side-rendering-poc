'use strict';

const fs = require('fs');
const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
const React = require('react');
const ReactDOM = require('react-dom/server');
const store = require('./store');
const rr = require('react-redux');

const HTTP_PORT = 4000;

// Components
const Alert = require('./components/alert');

// Body parser
app.use(bodyParser.json());

// Views
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use('/dist', express.static('dist'));

/**
 * Routes
 */
app.get('/', (req, res) => {
  let alert = ReactDOM.renderToString(React.createElement(rr.Provider, { store: store.init() }, React.createElement(Alert, { text: '1. Alert' })));

  let alert2 = ReactDOM.renderToString(React.createElement(rr.Provider, { store: store.init() }, React.createElement(Alert, { text: '2. Alert' })));

  res.render('index', { alert, alert2 });
});

app.listen(HTTP_PORT, () => {
  console.log(`--> Server running at port: ${HTTP_PORT}`)
});
