const path = require('path');

module.exports = {
  entry: './entry.js',
  output: {
    filename: 'main.js',
    path: path.join(__dirname, './dist/')
  },
  module: {
    loaders: [
      {
        test: /\.js?$/,
        loader: 'babel',
        exclude: /node_modules/
      }
    ]
  },
  plugins: [

  ],
  resolve: {
    modulesDirectories: [
      'node_modules',
      'lib'
    ]
  }
};
